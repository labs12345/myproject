package com.kdp.form;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

import com.kdp.Main;

public class UploadFromInternetFrame extends JFrame {
    private JTextField urlTextField;

    public UploadFromInternetFrame() {
        super("Загрузка из интернета");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        createGUI();
        setSize(600, 50);
        setVisible(true);
        setLocationRelativeTo(null);
    }

    private void createGUI() {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.LEFT));
        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        urlTextField = new JTextField("", 35);
        panel.add(new JLabel("Адрес:"));
        panel.add(urlTextField);

        JButton buttonUpload = new JButton("Загрузить");

        buttonUpload.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    URL url = new URL(urlTextField.getText());
                    Scanner s = new Scanner(url.openStream());

                    Main.createDiagramFromFile(s);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
        panel.add(buttonUpload);
        getContentPane().add(panel);
    }

}
