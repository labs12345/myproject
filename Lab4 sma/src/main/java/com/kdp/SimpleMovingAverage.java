package com.kdp;

import java.util.LinkedList;
import java.util.Queue;

public class SimpleMovingAverage {

    private final Queue<Double> Dataset = new LinkedList<>();
    private final int period;
    private double sum;

    public SimpleMovingAverage(int period) {
        this.period = period;
    }


    public void addData(double num) {
        sum += num;
        Dataset.add(num);

        if (Dataset.size() > period) {
            sum -= Dataset.remove();
        }
    }

    public double getMean() {
        return sum / period;
    }
}
