package model;

public class Mail {
    private String from;
    private String to;
    private Double xDSPAMProbability;
    private Double xDSPAMConfidence;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Double getxDSPAMProbability() {
        return xDSPAMProbability;
    }

    public void setxDSPAMProbability(Double xDSPAMProbability) {
        this.xDSPAMProbability = xDSPAMProbability;
    }

    public Double getxDSPAMConfidence() {
        return xDSPAMConfidence;
    }

    public void setxDSPAMConfidence(Double xDSPAMConfidence) {
        this.xDSPAMConfidence = xDSPAMConfidence;
    }

    @Override
    public String toString() {
        return "model.Mail{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", xDSPAMProbability='" + xDSPAMProbability + '\'' +
                ", xDSPAMConfidence='" + xDSPAMConfidence + '\'' +
                '}';
    }
}
