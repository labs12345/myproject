package com.kdp.sqrt;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input Operand: ");
        int operand = Integer.parseInt(scanner.nextLine());

        System.out.println("Result sqrt: " + sqrt(operand));
    }

    /**
     * Square root calculation
     *
     * @param a - operand for calculation
     */
    private static double sqrt(long a) {
        double b = a;
        int i = 0;
        while ((b * b > a) && (i < 200)) {
            b = (b + a / b) / 2;
            i++;
        }
        return b;
    }
}
